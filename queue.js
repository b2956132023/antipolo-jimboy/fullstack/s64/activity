let collection = [];

let index1 = 0;
let index2 = 0; 


function print() {

  const result = [];

  for (let i = index1; i < index2; i++) {
    result[result.length] = collection[i];
  }

  return result;
}



function enqueue(item) {

  collection[index2] = item;
  index2++;

  return print();
}



function dequeue() {

  if (index1 === index2) {

    return print();
  }

  const frontItem = collection[index1];
  index1++;

  const newCollection = {};

  let newIndex = 0;

  for (let i = index1; i < index2; i++) {
    newCollection[newIndex] = collection[i];
    newIndex++;
  }

  collection = newCollection;
  index2 = newIndex;
  index1 = 0;

  return print();
}



function size() {
  return index2 - index1;
}



function front() {
    return collection[0];
}



function isEmpty() {
    return collection.length === 0;
}



module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};